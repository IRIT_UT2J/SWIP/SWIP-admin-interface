import { SwipAdminPage } from './app.po';

describe('swip-admin App', () => {
  let page: SwipAdminPage;

  beforeEach(() => {
    page = new SwipAdminPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
