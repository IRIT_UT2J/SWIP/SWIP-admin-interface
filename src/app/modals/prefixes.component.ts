import { Component, Input, OnInit } from '@angular/core';

@Component({  
    selector: 'prefixes',
    templateUrl: './prefixes.component.html',
    styleUrls: ['./prefixes.component.css']
})

export class PrefixesComponent implements OnInit{
  @Input() prefixes: any;

  constructor() {}

  ngOnInit() : void {
  	console.log("prefixes : ", this.prefixes);
  }
}