import { Component, Input, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { PatternsComponent } from '../patterns/patterns.component';

@Component({  
    selector: 'confirm',
    templateUrl: './confirm.component.html',
    styleUrls: ['./loading.component.css']
})

export class ConfirmComponent {
  @Input() title: string;
  @Input() message: string;
  @Input() callback: any;
  @Input() param: any;
  @ViewChild('confirm') public confirm: ModalDirective;

  constructor(private parent: PatternsComponent) {}

  confirmAction() : void {
  	this.parent[this.callback](this.param);
  }

  ngAfterViewInit() : void {
    this.confirm.show();
  }
}