import { Component, Input, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { GetPatternsService } from '../patterns/get-patterns.service';
import { PatternsComponent } from '../patterns/patterns.component';
import { Pattern } from '../patterns/pattern';
import { Project } from '../projects/project';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({  
    selector: 'add-pattern',
    templateUrl: './add-pattern.component.html',
    styleUrls: ['./add-to-patterns.component.css']
})

export class AddPatternComponent implements OnInit, AfterViewInit {
  @Input() current_project: Project;
  @ViewChild('add_pattern') public add_pattern: ModalDirective;
  private patternName: string = '';
  private sentenceTemplate: string = '';
  private patterns: Pattern[];

  constructor(private patternsService: GetPatternsService,
              private parent: PatternsComponent) {}

  submitPattern() : void {
    if (this.patternName != '' && this.sentenceTemplate != '') {
        this.patterns.push({
            name: this.patternName,
            sentenceTemplate: this.sentenceTemplate,
            subpatterns: [],
            color: '',
            show_details: false,
            show_edit_options: false
          });
        console.log("new patterns : ", this.patterns);
        this.patternsService.setPatterns(this.current_project.projectName, {patterns: this.patterns})
        .subscribe(stuff => {
          this.add_pattern.hide();
          this.parent.getGeneralGraph();
        });
       
    } else {
      console.log("i should print an error message but i'm too lazy to do it now")
    }
  }

  ngOnInit() : void {
    this.patternsService.getPatterns(this.current_project.projectName).subscribe(patterns => this.patterns = patterns)
  }

  ngAfterViewInit() : void {
    this.add_pattern.show();
  }

}