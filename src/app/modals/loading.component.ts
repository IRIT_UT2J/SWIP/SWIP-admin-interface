import { Component, Input, OnInit } from '@angular/core';

@Component({  
    selector: 'loading',
    templateUrl: './loading.component.html',
    styleUrls: ['./loading.component.css']
})

export class LoadingComponent {
  @Input() message: string;

  constructor() {}

}