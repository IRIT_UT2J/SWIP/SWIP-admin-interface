import { Component, Input, OnInit, OnChanges, SimpleChanges, AfterViewInit, ViewChild } from '@angular/core';
import { GetPatternsService } from '../patterns/get-patterns.service';
import { PatternsComponent } from '../patterns/patterns.component';
import { Pattern } from '../patterns/pattern';
import { Project } from '../projects/project';
import { Subpattern } from '../patterns/subpattern';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { QueryService } from '../queries/query.service';
import { AddSubpatternComponent } from './add-subpattern.component'; 

import { Observable }        from 'rxjs/Observable';
import { Subject }           from 'rxjs/Subject';

// Observable class extensions
import 'rxjs/add/observable/of';

// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@Component({  
    selector: 'add-triple',
    templateUrl: './add-triple.component.html',
    styleUrls: ['./add-to-patterns.component.css']
})

export class AddTripleComponent implements OnInit {
  @Input() current_project: Project;
  @Input() subpattern: Subpattern;
  @Input() parentSubpattern: any;
  @ViewChild('add_triple') public add_triple: ModalDirective;
  private patterns: Pattern[];

  private selected_subject: string = '';
  private selected_predicate: string = '';
  private selected_object: string = '';
  private subjects: string[] = [];
  private predicates: string[] = [];
  private objects: string[] = [];
  private mapSuggestionLabelToUri = {};
  private searchSubject = new Subject<string>();
  private searchPredicate = new Subject<string>();
  private searchObject = new Subject<string>();
  private checkbox_subject: boolean = true;
  private checkbox_predicate: boolean = true;
  private checkbox_object: boolean = true;

  private spId: string = '';
  private minOccurences: number = 0;
  private maxOccurences: number = 1;
  private canonicaleType: string = "fr.irit.swip2.pivottomappings.model.patterns.subpattern.PatternTriple";

  constructor(private patternsService: GetPatternsService,
              private queryService: QueryService,
              private parent: PatternsComponent) {}

  submitTriple() : void {
    console.log("in submit triple")
    let e1 = {
      canonicaleType: 'fr.irit.swip2.pivottomappings.model.patterns.patternElement.ClassPatternElement',
      id: 1,
      mappingCompulsory: false,
      maxOccurences: 1, // CHANGE THIS
      name: this.selected_subject,
      qualifying: this.checkbox_subject,
      uri: this.mapSuggestionLabelToUri[this.selected_subject]
    };

    let e2 = {
      canonicaleType: 'fr.irit.swip2.pivottomappings.model.patterns.patternElement.PropertyPatternElement',
      id: 2,
      mappingCompulsory: false,
      maxOccurences: 1, // CHANGE THIS
      name: this.selected_predicate,
      qualifying: this.checkbox_predicate,
      uri: this.mapSuggestionLabelToUri[this.selected_predicate]
    };
    let object_uri = this.mapSuggestionLabelToUri[this.selected_object] ? this.mapSuggestionLabelToUri[this.selected_object] : this.selected_object;
    let e3 = {
      canonicaleType: 'fr.irit.swip2.pivottomappings.model.patterns.patternElement.ClassPatternElement',
      id: 3,
      mappingCompulsory: false,
      maxOccurences: 1, // CHANGE THIS
      name: this.selected_object,
      qualifying: this.checkbox_object,
      uri: object_uri
    };

    let triple = {
      canonicaleType: this.canonicaleType,
      e1: e1,
      e2: e2,
      e3: e3
    };

    console.log("TRIPLE : ", triple);

    if (this.subpattern) {
      console.log("OLD SUBPATTERN : ", this.subpattern)
      this.subpattern.subpatterns.push(triple);
      console.log("NEW SUBPATTERN : ", this.subpattern);

      for (let pattern of this.patterns) {
        this.updateSubpattern(pattern, this.subpattern);
      };
      console.log("new patterns : ", this.patterns);

      this.patternsService.setPatterns(this.current_project.projectName, {patterns: this.patterns})
      .subscribe(stuff => {
        this.parent.getGeneralGraph();
      });
    } else { // the subpattern is being created
      console.log("ain't got no parents, bruh");
      this.parentSubpattern.setTriple(triple);
    }
    
    this.hideModal();
  }

  updateSubpattern(element: any, subpattern : Subpattern) {
    element.subpatterns.forEach( item => {
        if (item.subpatterns)
          this.updateSubpattern(item, subpattern);
        if (item.id == subpattern.id) {
          let index = element.subpatterns.indexOf(item);
          element.subpatterns[index] = subpattern;
        }
      });
  }

  getSubjectSuggestions() : void {
    this.queryService.getSubjectOfTriple(this.current_project, "")
      .subscribe(res => {
        this.subjects = [];
        for (let resultat of res) {
          let label;
          if (resultat.class.type == 'uri')
            label = resultat.class.value.split("#")[1];
          else
            label = resultat.class.value;

          this.mapSuggestionLabelToUri[label] = resultat.class.value;
          this.subjects.push(label);
        };
        this.mapSuggestionLabelToUri["owl:Thing"] = "http://www.w3.org/2002/07/owl#Thing";
        this.subjects.push("owl:Thing");
      });
  }

  getPredicateSuggestions() : void {
    this.queryService.getPredicateOfTriple(this.current_project, '', this.mapSuggestionLabelToUri[this.selected_subject])
      .subscribe(res => {
        this.predicates = [];
        for (let resultat of res) {
          let label;
          if (resultat.predicate.type == 'uri')
            label = resultat.predicate.value.split("#")[1];
          else
            label = resultat.predicate.value;

          this.mapSuggestionLabelToUri[label] = resultat.predicate.value;
          this.predicates.push(label);
        };
      });
  }

  getObjectSuggestions() : void {
    this.queryService.getObjectOfTriple(this.current_project, '', this.mapSuggestionLabelToUri[this.selected_subject], this.mapSuggestionLabelToUri[this.selected_predicate])
      .subscribe(res => {
        this.objects = [];
        for (let resultat of res) {
          let label;
          if (resultat.object.type == 'uri')
            label = resultat.object.value.split("#")[1];
          else
            label = resultat.object.value;

          this.mapSuggestionLabelToUri[label] = resultat.object.value;
          this.objects.push(label);
        };
      });
  }

  updateSuggestions() : void {
    this.searchSubject.next(this.selected_subject);
    this.searchPredicate.next(this.selected_predicate);
    this.searchObject.next(this.selected_object);
  }

  updateSubjectSparqlQuery() : void {
    this.searchSubject
      .debounceTime(500)        // wait 300ms after each keystroke before considering the term
      .distinctUntilChanged()   // ignore if next search term is same as previous
      .switchMap(term => {
        if (this.selected_subject == '')
          this.getSubjectSuggestions();
        return term   // switch to new observable each time the term changes
        // return the http search observable
        ? this.queryService.getSubjectOfTriple(this.current_project, term)
        // or the observable of empty projects if there was no search term
        : [];})
        .catch(error => {
          console.log(error);
          return [];
        })
        .subscribe(res => {
          this.subjects = [];
          for (let resultat of res) {
            let label;
            if (resultat.class.type == 'uri')
              label = resultat.class.value.split("#")[1];
            else
              label = resultat.class.value;

            this.mapSuggestionLabelToUri[label] = resultat.class.value;
            this.subjects.push(label);
          };
          this.mapSuggestionLabelToUri["owl:Thing"] = "http://www.w3.org/2002/07/owl#Thing";
          this.subjects.push("owl:Thing");
        });
  }

  updatePredicateSparqlQuery() : void {
    console.log("in update pred")
    this.searchPredicate
      .debounceTime(500)        // wait 300ms after each keystroke before considering the term
      .distinctUntilChanged()   // ignore if next search term is same as previous
      .switchMap(term => {
        if (this.selected_predicate == '')
          this.getPredicateSuggestions();
        return term   // switch to new observable each time the term changes
        // return the http search observable
        ? this.queryService.getPredicateOfTriple(this.current_project, term, this.mapSuggestionLabelToUri[this.selected_subject])
        // or the observable of empty projects if there was no search term
        : [];})
        .catch(error => {
          console.log(error);
          return [];
        })
        .subscribe(res => {
          this.predicates = [];
          for (let resultat of res) {
            let label;
            if (resultat.predicate.type == 'uri')
              label = resultat.predicate.value.split("#")[1];
            else
              label = resultat.predicate.value;

            this.mapSuggestionLabelToUri[label] = resultat.predicate.value;
            this.predicates.push(label);
          };
        });
  }

  updateObjectSparqlQuery() : void {
    this.searchObject
      .debounceTime(500)        // wait 300ms after each keystroke before considering the term
      .distinctUntilChanged()   // ignore if next search term is same as previous
      .switchMap(term => {
        if (this.selected_object == '')
          this.getObjectSuggestions();
        return term   // switch to new observable each time the term changes
        // return the http search observable
        ? this.queryService.getObjectOfTriple(this.current_project, term, this.mapSuggestionLabelToUri[this.selected_subject], this.mapSuggestionLabelToUri[this.selected_predicate])
        // or the observable of empty projects if there was no search term
        : [];})
        .catch(error => {
          console.log(error);
          return [];
        })
        .subscribe(res => {
          this.objects = [];
          for (let resultat of res) {
            let label;
            if (resultat.object.type == 'uri')
              label = resultat.object.value.split("#")[1];
            else
              label = resultat.object.value;

            this.mapSuggestionLabelToUri[label] = resultat.object.value;
            this.objects.push(label);
          };
        });
  }

  hideModal() : void {
    console.log("in hide modal !")
    if (this.subpattern)
      this.parent.hideModals();
    else {
      console.log("hiding...")
      this.parentSubpattern.hideTripleModal();
    }
  }

  ngOnInit() : void {
    this.patternsService.getPatterns(this.current_project.projectName).subscribe(patterns => this.patterns = patterns)
    console.log("adding a triple to ", this.subpattern)
    this.getSubjectSuggestions();
    this.updateSubjectSparqlQuery();
  }

  ngOnChanges(changes: SimpleChanges) : void {
    console.log("changes : ", changes)
    if (!changes.subpattern.firstChange)
      this.add_triple.show();
  }

  ngAfterViewInit() : void {
    this.add_triple.show();
  }
}