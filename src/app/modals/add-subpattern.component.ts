import { Component, Input, OnInit, OnChanges, SimpleChanges, AfterViewInit, ViewChild, Injectable } from '@angular/core';
import { GetPatternsService } from '../patterns/get-patterns.service';
import { PatternsComponent } from '../patterns/patterns.component';
import { Pattern } from '../patterns/pattern';
import { Project } from '../projects/project';
import { Subpattern } from '../patterns/subpattern';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AddTripleComponent } from '../modals/add-triple.component'; 

@Component({  
    selector: 'add-subpattern',
    templateUrl: './add-subpattern.component.html',
    styleUrls: ['./add-to-patterns.component.css']
})

export class AddSubpatternComponent implements OnInit, AfterViewInit, OnChanges {
  @Input() current_project: Project;
  @Input() pattern: any; // can also be a subpattern ?
  @ViewChild('add_triple') public add_triple: ModalDirective;
  @ViewChild('add_subpattern') public add_subpattern: ModalDirective;
  @ViewChild('addTriple') addTriple; 

  private spId: string = '';
  private minOccurrences: number = 0;
  private maxOccurrences: number = 1;
  private pivotElement;
  private canonicaleType: string = "fr.irit.swip2.pivottomappings.model.patterns.subpattern.SubpatternCollection";
  private patterns: Pattern[];
  private triple: any;
  private showTriple : boolean = false;

  // show_details = false;
  // show_edit_options = false;
  // subpatterns = [this.triple]
  // pivotElement = this.triple.e1

  constructor(private patternsService: GetPatternsService,
              private parent: PatternsComponent) {}

  setTriple(triple: any) : void {
    console.log("coucou !")
    this.triple = triple;
    this.pivotElement = triple.e1;
    this.addTriple.nativeElement.disabled = true;
  }

  updatePatterns(element: any, subpattern : any, sp: Subpattern) {
    element.subpatterns.forEach( item => {
        if (item.subpatterns)
          this.updatePatterns(item, subpattern, sp);
        if (item.id && item.id == subpattern.id) { // undefined == undefined, bruh
          item.subpatterns.push(sp);
        }
      });
  }

  submitSubpattern() : void {
    let subpattern = {
      pivotElement: this.pivotElement,
      minOccurences: this.minOccurrences,
      maxOccurences: this.maxOccurrences,
      id: this.spId,
      canonicaleType: this.canonicaleType,
      subpatterns: [this.triple],
      show_details: false,
      show_edit_options: false
    };

    for (let pattern of this.patterns) {
      if (pattern.name == this.pattern.name) {
        pattern.subpatterns.push(subpattern);
        break;
      }
      this.updatePatterns(pattern, this.pattern, subpattern);
    };
      console.log("new patterns : ", this.patterns);

      this.patternsService.setPatterns(this.current_project.projectName, {patterns: this.patterns})
      .subscribe(stuff => {
        this.parent.getGeneralGraph();
        this.add_subpattern.hide();
      });
  }

  hideTripleModal() : void {
    this.showTriple = undefined;
  }

  ngOnChanges(changes: SimpleChanges) : void {
    console.log("CHANGES : ", changes);
    if (!changes.pattern.firstChange)
      this.add_subpattern.show();
  }

  ngOnInit() : void {
    console.log("adding a subpattern !");
    this.patternsService.getPatterns(this.current_project.projectName).subscribe(patterns => this.patterns = patterns);
  }

  ngAfterViewInit() : void {
    this.add_subpattern.show();
  }

}