import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { GetProjectsService } from '../projects/get-project.service';
import { QueryService } from './query.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { LoadingComponent } from '../modals/loading.component'; 
import { Project } from '../projects/project';
import { Mapping } from './mapping';

@Component({
	selector: 'nl-query',
	templateUrl: './nl-query.component.html',
	styleUrls: ['./nl-query.component.css']
})

export class NLQueryComponent implements OnInit {
	@ViewChild('loading') public loading: ModalDirective;
	@Input() project: Project;
	private nl_query: string = '';
	private pivotQuery: string; // pivot query of the current query
	private mappings: Mapping[]; // mappings of the current query
	private no_mappings: Boolean = false; // is used to show a specific message if there's no mappings
	private current_mapping: Mapping;  // mapping selected by the user

	constructor(
	  private getProjectsService: GetProjectsService,
	  private queryService: QueryService,
	  private route: ActivatedRoute
	) {}

	/** 
	* Returns the pivot query and the mappings of the specified query.
	* @param {string} query - the query of the user
	*/
	getQueryResults(query: string): void {
		this.loading.show();
		this.queryService.getQueryResults(this.project.projectName, query)
			.subscribe(query => {
				if (query) {
					console.log("query : ", query);
					this.loading.hide();
					this.pivotQuery = query.pivotQuery;
					this.mappings = query.mappings;
					this.changeMappings();
				} else {
					this.no_mappings = true; // need to make a better error message
					this.loading.hide();
				}
			});
		
	}

	/** 
	* Changes the mappings of the current query : checks if there's at least one mapping, else prints a message.
	* For each mapping, sets the generalization level of variables and generate a descriptive sentence.
	* Gets the Sparql query of the first mapping.
	*/
	changeMappings(): void {
		console.log("MAPPINGSSSSS : ", this.mappings);
		if (!this.mappings) {
			this.no_mappings = true;  // prints something like "sorry, we don't know the answer"
			this.loading.hide();
			this.current_mapping = null;
			return;
		}

		this.no_mappings = false;
		this.mappings.forEach(mapping => {
			mapping.descriptiveSentence.generalisation.gen_levels = {}; // init a dict containing, for each variable,
																		// its number of generalization and its current gen level
			for (let gen in mapping.descriptiveSentence.generalisation) { // for each variable which can be generalized
				if (gen != "gen_levels") {
					let level_max = mapping.descriptiveSentence.generalisation[gen].length;
					mapping.descriptiveSentence.generalisation.gen_levels[gen] = {};
					mapping.descriptiveSentence.generalisation.gen_levels[gen].max = level_max-1;
					mapping.descriptiveSentence.generalisation.gen_levels[gen].current = 0;  // by default, the variable is gonna be
																							 // set with its most specified value
				}
			};
			mapping.generated_sentence = this.queryService.generateDescriptiveSentence(mapping);
		});
		console.log("NEW MAPPINGSSSS : ", this.mappings);
		this.current_mapping = this.mappings[0]; // the current mapping is the most pertinent
		this.getSparqlQuery(this.current_mapping);
	}

	/** 
	* Get the Sparql query results of the specified mapping.
	* @param {Mapping} mapping - the mapping to which the Sparql query belongs
	*/
	getSparqlQuery(mapping: Mapping): void {
		this.loading.show();
		this.queryService.getSparqlQueryResults(this.project.projectName, mapping)
			.subscribe(results => {
				this.getSparqlAnswers(mapping, results);
				this.current_mapping = mapping;
			});
	}

	/** 
	* Prints the results of the Sparql query
	* @param {Mapping} mapping - the mapping to which the Sparql query belongs
	* @param {any} queryResults - the results of the Sparql query
	*/
	getSparqlAnswers(mapping: Mapping, queryResults: any) {
		console.log("QUERY : ", queryResults);
		if (!queryResults) {
			mapping.answer = "An error occurred...";
			this.loading.hide();
			return;
		}
		let sparql_results: any;
		if (queryResults.results) { // if it's a SELECT Sparql query
			sparql_results = queryResults.results.bindings;
			this.getAnswerLabels(sparql_results);  // get the labels of the results
		} else {  // else, it's an ASK Sparql query
			sparql_results = queryResults.boolean ? "It exists" : "It doesn't exist";
		}
		mapping.answer = sparql_results;  // print the results
		this.loading.hide();
	}

	/** 
	* Gets the labels of the specified answer
	* @param {any} answer - the answer of which we want the labels
	*/
	getAnswerLabels(answer): void {
		answer.forEach(response => {
			for (var key in response) {
				if (response[key].type == "uri") {
					response.uri = response[key].value;
					this.queryService.getUriLabel(this.project.projectName, response[key].value)
					.subscribe(label => response.labels = label)
				} else {
					response.labels = response[key].value;
				}
			}
		});
	}

	/** 
	* Updates the query answers with a new generalization value chosen with a slider.
	* @param {number} gen_level - the generalization level of the specified variable : if a variable 'gen3' has 3
	*                             possible generalizations, gen3[0] is the most specific value, and gen3[2] is most general
	* @param {Mapping} mapping - the mapping of which the descriptive sentence and Sparql query are generated
	* @param {string} gen - the variable which is going to be generalized
	*/
	onGenChanged(gen_level: number, mapping: Mapping, gen: string) {
	    mapping.generated_sentence = this.queryService.generateDescriptiveSentenceGen(mapping, gen_level, gen); // get the new descriptive sentence
		this.queryService.getSparqlQueryResultsGen(this.project.projectName, mapping, gen_level, gen)
			.subscribe(query => this.getSparqlAnswers(mapping, query));  // get the answer of the new Sparql query
	  }

	/**
	* Checks if the given answer belongs to a Sparql query of type 'ask'.
	*/
	isAsk(answer: any): boolean {
		return typeof answer === 'string';
	}

	ngOnInit(): void {
		this.route.params.subscribe(
        (params : Params) => {
            this.getProjectsService.getProject(params["projectName"])
				.subscribe(project => {
					this.project = project;
					this.current_mapping = null;
					this.mappings = null;
					this.nl_query = '';
				});  // get the current project
    	});

	}
}