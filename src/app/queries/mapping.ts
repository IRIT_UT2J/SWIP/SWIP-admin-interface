export class Mapping {
	relevanceMark: string;
	sparqlQuery: any;
	descriptiveSentence: any;
	mappingDescription: string;
	generated_sentence: any;  // string = "";
	answer: any;
	show_desc: boolean = false;
}