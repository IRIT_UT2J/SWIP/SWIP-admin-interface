import { Injectable } from '@angular/core';
import { Headers, Http, Response, URLSearchParams } from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import { Mapping } from './mapping';
import { Project } from '../projects/project';
import { GetProjectsService } from '../projects/get-project.service';
import { ServerService } from '../main/server.service';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable() 
export class QueryService {

	private url : string = this.serverService.getServerUrl();
	private headers = new Headers({'Content-Type': 'application/json'});

	constructor(private http: Http,
				private getProjectsService: GetProjectsService,
				private serverService: ServerService) { }

	/** 
	* Returns the pivot query and the mappings of the specified query.
	* @param {string} project - the name of the current project
	* @param {string} query - the query of the user
	*/
	getQueryResults(project: string, query: string): Observable<any> {
	let params = new URLSearchParams();
	params.set('q', query);
	return this.http.get(`${this.url}/${project}/ask`, { search: params })
		.map((res:Response) => res.json())
	    .catch((err:any) => this.handleError());
	}

	/** 
	* Returns the descriptive sentence generated with the most specified value of each variable ({{gen1}}, {{gen3}}...)
	* @param {Mapping} mapping - the mapping of which the descriptive sentence is generated
	*/
	generateDescriptiveSentence(mapping: Mapping): string {
		return this.sentenceGenerator(mapping, "descriptiveSentence");
	}

	/** 
	* Returns the descriptive sentence generated with a given generalization level for a given variable
	* @param {Mapping} mapping - the mapping of which the descriptive sentence is generated
	* @param {number} gen_level - the generalization level of the specified variable : if a variable 'gen3' has 3
	*                             possible generalizations, gen3[0] is the most specific value, and gen3[2] is most general
	* @param {string} gen - the variable which is going to be generalized
	*/
	generateDescriptiveSentenceGen(mapping: Mapping, gen_level: number, gen: string): string {
		return this.sentenceGeneratorWithGens(mapping, "descriptiveSentence", gen_level, gen);
	}

	/** 
	* Generates the Sparql query of the given mapping with the most specified value of each variable ({{gen1}}, {{gen3}}...)
	* and returns the results of this generated Sparql query
	* @param {string} project - the name of the current project
	* @param {Mapping} mapping - the mapping of which the Sparql query is generated
	*/
	getSparqlQueryResults(project: string, mapping: Mapping): Observable<any> {
		let sentence = this.sentenceGenerator(mapping, "sparqlQuery"); // generate the Sparql query
		return this.askSparqlQuery(project, sentence);
	}

	/** 
	* Generates the Sparql query of the mapping with a given generalization level for a given variable
	* and returns the results of this generated Sparql query
	* @param {string} project - the name of the current project
	* @param {Mapping} mapping - the mapping of which the Sparql query is generated
	* @param {number} gen_level - the generalization level of the specified variable : if a variable 'gen3' has 3
	*                             possible generalizations, gen3[0] is the most specific value, and gen3[2] is most general
	* @param {string} gen - the variable which is going to be generalized
	*/
	getSparqlQueryResultsGen(project: string, mapping: Mapping, gen_level: number, gen : string): Observable<any> {
		let sentence = this.sentenceGeneratorWithGens(mapping, "sparqlQuery", gen_level, gen);
		return this.askSparqlQuery(project, sentence);
	}

	/** 
	* Asks the specified Sparql query to the project's Sparql endpoint
	* @param {string} project - the name of the current project
	* @param {string} sentence - the Sparql query
	*/
	askSparqlQuery(project: string, sentence: string) {
		let params = new URLSearchParams();
		params.set('q', sentence);
		params.set('format', 'json');

		return this.http.get(`${this.url}/${project}/sparql`, { search: params })
			.map((res:Response) => res.json())
		    .catch((err:any) => this.handleError());
	}

	/** 
	* Generates a descriptive sentence or a Sparql query from the specified mapping
	* @param {Mapping} mapping - the mapping of which the Sparql query or the descriptive sentence is generated
	* @param {string} type - the type of the sentence to generate : descriptive sentence or Sparql query
	*/
	sentenceGenerator(mapping: Mapping, type: string): string {
		let gen = mapping[type].generalisation;  // get the array of available generalizations
		let sentence = mapping[type].string;  // get the sentence to generate
		let matches = sentence.match(/({{[a-z]+[0-9]+}})/g);  // get all the occurrences of variables not generated
		if (!matches)
			return sentence.replace(/@fr/g, '');

		matches.forEach(match => {  // for each variable to generate
			let new_interpolation = match.replace('{{', '').replace('}}', '');  // get the name of the variable
			if (gen[new_interpolation])
				sentence = sentence.replace(match, gen[new_interpolation][0]);  // change that variable with its most specified value
		});
		return sentence.replace(/@fr/g, '');
	}

	/** 
	* Generates a descriptive sentence or a Sparql query with a given generalization level for a given variable
	* @param {Mapping} mapping - the mapping of which the Sparql query or the descriptive sentence is generated
	* @param {string} type - the type of the sentence to generate : descriptive sentence or Sparql query
	* @param {number} gen_level - the generalization level of the specified variable : if a variable 'gen3' has 3
	*                             possible generalizations, gen3[0] is the most specific value, and gen3[2] is most general
	* @param {string} gen - the variable which is going to be generalized
	*/
	sentenceGeneratorWithGens(mapping: Mapping, type: string, gen_level: number, generalisation: string): string {
		let gen_levels = mapping.descriptiveSentence.generalisation.gen_levels; // get the mappings' different variables with
																				// their current and max generalization level
		let gen = mapping[type].generalisation;  // get the array of available generalizations
		let sentence = mapping[type].string;  // get the sentence to generate
		
		let regex = new RegExp("{{"+generalisation+"}}","g");
		let matches_gen = sentence.match(regex);  // get all the occurrences of the given variable to generate
		if (matches_gen) {
			matches_gen.forEach(match => {
				sentence = sentence.replace(match, gen[generalisation][gen_level]);
			});
		}

		for (let current_gen in mapping.descriptiveSentence.generalisation) { // generate other variables from their current gen level
			if (current_gen != 'gen_levels' && current_gen != generalisation) {
				let level = gen_levels[current_gen].current;  // get its current gen level
				let current_regex = new RegExp("{{"+current_gen+"}}","g");
				let matches = sentence.match(current_regex);
				if (matches) {
					matches.forEach(match => {
				  		sentence = sentence.replace(match, gen[current_gen][level]);
				  	});
				}
			}
		}
		return sentence.replace(/@fr/g, '');
	}

	/** 
	* Returns the labels of the given URI ; if there's no labels, returns its fragment URI
	* @param {string} project - the name of the current project
	* @param {string} uri - the URI of which we get the labels
	*/
	getUriLabel(project: string, uri: string): Observable<any> {
		let query = `PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
					SELECT ?label {
					   <` + uri + `> rdfs:label ?label
					}`;

		let params = new URLSearchParams();
		params.set('q', query);
		params.set('format', 'json');
		return this.http.get(`${this.url}/${project}/sparql`, { search: params })
			.map((res:Response) => {
				let results = res.json().results.bindings;
				if (results.length == 0) { // if there's no labels, return the fragmeNT URI
					if (uri.split("#")[1]) {// if it's an uri
						results = uri.split("#")[1];
				}
					else // it's a literal
						results = uri;
				}
				return results;
			})
		    .catch((err:any) => this.handleError(uri));

	}

	getSubjectOfTriple(project: Project, sentence: string) : Observable<any> {
		console.log("gonna get subject of ", sentence);
		let query = `PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
					 PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
					 PREFIX owl: <http://www.w3.org/2002/07/owl#>
					SELECT DISTINCT ?class { 
					   ?class rdf:type owl:Class.
					   ?class rdfs:label ?label. 
					   filter contains(lcase(str(?label)), "` + sentence.toLowerCase() + `")
					} LIMIT 100`;

		let params = new URLSearchParams();
		let sparqlEndpoint: string;
		if (project.distant) {
			sparqlEndpoint = project.kbUri + '/sparql';
			params.set('query', query);
		}
		else {
			sparqlEndpoint = this.url + '/' + project.projectName + '/sparql';
			params.set('q', query);
		}
		
		params.set('format', 'json');
		return this.http.get(sparqlEndpoint, { search: params })
			.map((res:Response) => {
				console.log("get subject : ", res.json().results.bindings);
				return res.json().results.bindings;
			})
		    .catch((err:any) => this.handleError(sentence));
	}

	getPredicateOfTriple(project: Project, term: string, subjectUri: string) : Observable<any> {
		let query = `PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
					 PREFIX owl: <http://www.w3.org/2002/07/owl#>
					SELECT DISTINCT ?predicate {
						{
							{?predicate a owl:DatatypeProperty} UNION {?predicate a owl:ObjectProperty}
	  						?predicate rdfs:label ?label. 
					   		filter contains(lcase(str(?label)), "` + term.toLowerCase() + `")
	  					} UNION {<` + subjectUri + `> ?predicate ?object .}
					} LIMIT 100
					`;

		let params = new URLSearchParams();
		let sparqlEndpoint: string;
		if (project.distant) {
			sparqlEndpoint = project.kbUri + '/sparql';
			params.set('query', query);
		}
		else {
			sparqlEndpoint = this.url + '/' + project.projectName + '/sparql';
			params.set('q', query);
		}
		
		params.set('format', 'json');
		return this.http.get(sparqlEndpoint, { search: params })
			.map((res:Response) => {
				console.log("get predicate : ", res.json().results.bindings);
				return res.json().results.bindings;
			})
		    .catch((err:any) => this.handleError(subjectUri));
	}

	getObjectOfTriple(project: Project, term: string, subjectUri: string, predicateUri: string) : Observable<any> {
		let query = `PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
					SELECT DISTINCT ?object {
						{
	  						?object rdfs:label ?label. 
					   		filter contains(lcase(str(?label)), "` + term.toLowerCase() + `")
	  					} UNION {<` + subjectUri + `> <` + predicateUri + `> ?object .}
					} LIMIT 50`;

		let params = new URLSearchParams();
		let sparqlEndpoint: string;
		if (project.distant) {
			sparqlEndpoint = project.kbUri + '/sparql';
			params.set('query', query);
		}
		else {
			sparqlEndpoint = this.url + '/' + project.projectName + '/sparql';
			params.set('q', query);
		}
		
		params.set('format', 'json');
		
		return this.http.get(sparqlEndpoint, { search: params })
			.map((res:Response) => {
				console.log("get object : ", res.json().results.bindings);
				return res.json().results.bindings;
			})
		    .catch((err:any) => this.handleError(subjectUri));
	}

	/** 
	* Returns the prefix of the given URI
	* @param {string} uri - the URI of which we get the prefix
	*/
	getUriPrefix(uri: string): Observable<any> {
		let url = `http://prefix.cc/reverse?uri=${uri}&format=json`;
		let encoded = url.replace('#', "%23");
		return this.http.get(encoded)
			.map((res:Response) => res.url.replace('http://prefix.cc/', ''))
		    .catch((err:any) => this.handleError(uri));
	}

	/** 
	* Returns the given prefix mapped to its URI
	* @param {any} map - a dict mapping a prefix to its URI
	* @param {string} prefix - the prefix to add to the map
	*/
	mapPrefixToUri(map, prefix: string): Observable<any> {
		let url = `http://prefix.cc/${prefix}.file.json`;
		let encoded = url.replace('#', "%23");
		return this.http.get(encoded) // get the prefix URI
			.map((res:Response) => {
				map[prefix] = res.json()[prefix]; // add them to the map
				return map;
			})
		    .catch((err:any) => this.handleError(prefix));
	}

	/** 
	* Prints an error log and returns the specified value
	* @param {any} elementToReturn - the element to return, which can be undefined
	*/
	handleError(elementToReturn = undefined) : Observable<any> {
		console.log("An error occurred..."); // needs to be a slight bit more explicit
		return Observable.of(elementToReturn);
	}

}