import { Component, OnInit, Input } from '@angular/core';
import { Project } from '../projects/project';
import { GetProjectsService } from '../projects/get-project.service';

@Component({
	selector: 'navbar',
	templateUrl: './navbar.component.html',
	styleUrls: ['./navbar.component.css']
})

export class NavbarComponent {
	@Input() current_project: string;
	private projects: Project[];
	private isCollapsed : Boolean = true; // make the navbar responsive

	constructor(private getProjectsService: GetProjectsService) { }

	ngOnInit(): void { // on init, get available projects
		this.getProjectsService.getProjects().subscribe(projects => this.projects = projects);
	}
}