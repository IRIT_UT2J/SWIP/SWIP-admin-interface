import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'keys' }) // returns the keys of a dict
export class KeysPipe implements PipeTransform {
  transform(value): any {
    if(!value) return null;
    return Object.keys(value);
  }
}