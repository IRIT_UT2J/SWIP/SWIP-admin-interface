import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Project } from './project';
import { GetProjectsService } from './get-project.service';

@Component({
  selector: 'projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})

export class ProjectsComponent implements OnInit {
  projects: Project[]; // array of available projects

  constructor(private getProjectsService: GetProjectsService,
              private router: Router) { }

  ngOnInit(): void {
  	this.getProjectsService.getProjects().subscribe(projects => {
  		this.projects = projects; // get all the available projects
  		console.log("PROJECTS : ", this.projects);
  	});
  }
}

