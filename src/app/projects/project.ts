export class Project {
	projectName: string;
	projectDesc: string;
	kbUri: string;
	distant: boolean;
}