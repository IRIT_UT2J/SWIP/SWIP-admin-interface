import { Injectable, } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Observable }     from 'rxjs/Observable';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { Project } from '../projects/project';
import { ServerService } from '../main/server.service';

@Injectable() 
export class GetProjectsService {

	  private url : string = this.serverService.getServerUrl();
	  private headers = new Headers({'Content-Type': 'application/json'});

	  constructor(private http: Http,
	  			  private serverService: ServerService) { }

	  /** 
	  * Returns all the projects currently available.
	  */  
	  getProjects() : Observable<Project[]> {
	    return this.http.get(`${this.url}/getProjects`)
	        .map((res:Response) => res.json().projects as Project[])
	        .catch((err:any) => this.handleError());
     }

     /** 
	  * Returns the project which has the specified name.
	  * @param {string} projectName - the name of the project to get
	  */  
     getProject(projectName: string): Observable<Project> {
     	return this.getProjects()
    	.mergeMap(processArray => {
	      let array = processArray.filter(project => project.projectName === projectName);
	      if (array.length === 0) 
	      	return Observable.throw('project not found');
	      return array;
	    })
	    .catch((err:any) => this.handleError());
	}

	/** 
	* Returns all the projects names that include the specified term.
	* @param {string} term - the term that we want to match with a project name
	*/  
     search(term: string): Observable<Project[]> {
     	return this.getProjects().filter(res => res[0].projectName.toLowerCase().includes(term.toLowerCase()));
     }

     /** 
	 * Prints an error log and returns an undefined value.
	 */
     handleError() : Observable<any> {
		console.log("An error occured..."); // needs to be a slight bit more explicit
		return Observable.of(undefined);
	}
}