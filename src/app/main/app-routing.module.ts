import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectsComponent }      from '../projects/projects.component';
import { ProjectSearchComponent }      from '../project-search/project-search.component';
import { NLQueryComponent }  from '../queries/nl-query.component';
import { PatternsComponent }  from '../patterns/patterns.component';

const routes: Routes = [
  { path: '', redirectTo: '/projects', pathMatch: 'full' },
  // { path: 'projectSearch', component: ProjectSearchComponent },
  { path: 'projects', component: ProjectsComponent },
  { path: 'project/:projectName', component: NLQueryComponent },
  { path: 'project/:projectName/patterns', component: PatternsComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}