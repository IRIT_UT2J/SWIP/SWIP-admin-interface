/* --- MODULES --- */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AlertModule, CollapseModule, ModalModule, TypeaheadModule } from 'ngx-bootstrap';
// import { CollapseDirective } from 'ng2-bootstrap';
import { DropdownModule } from "ngx-dropdown"; // dropdown in the navbar
import { BootstrapModalModule } from 'ng2-bootstrap-modal';  // delete this ?
import { NouisliderComponent } from 'ng2-nouislider'; // cursor slider for spec/gen
import { RouterModule }   from '@angular/router';

/* --- COMPONENTS --- */
import { AppComponent } from './app.component';
import { ProjectsComponent } from '../projects/projects.component';
import { ProjectSearchComponent } from '../project-search/project-search.component';
import { NavbarComponent } from '../navbar/navbar.component';
import { NLQueryComponent } from '../queries/nl-query.component';
import { PatternsComponent }  from '../patterns/patterns.component';
import { SpHierarchyComponent } from '../network/sp-hierarchy.component';
import { SubpatternsComponent } from '../network/subpatterns.component';
import { EditNetworkComponent } from '../network/edit-network.component';
import { AddPatternComponent } from '../modals/add-pattern.component'; 
import { AddSubpatternComponent } from '../modals/add-subpattern.component'; 
import { AddTripleComponent } from '../modals/add-triple.component'; 
import { LoadingComponent } from '../modals/loading.component'; 
import { ConfirmComponent } from '../modals/confirm.component'; 
import { PrefixesComponent } from '../modals/prefixes.component'; 

/* --- SERVICES --- */
import { GraphService } from '../network/create-graph.service';
import { GetPatternsService } from '../patterns/get-patterns.service';
import { QueryService } from '../queries/query.service'
import { GetProjectsService } from '../projects/get-project.service';
import { ServerService } from '../main/server.service';

/* --- PIPES --- */
import { KeysPipe } from '../pipes/keys.pipe';

/* --- ROUTING --- */
import { AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    ProjectsComponent,
    ProjectSearchComponent,
    NLQueryComponent,
    PatternsComponent,
    NavbarComponent,
    // CollapseDirective,
    NouisliderComponent,
    SpHierarchyComponent,
    SubpatternsComponent,
    EditNetworkComponent,
    AddPatternComponent,
    AddSubpatternComponent,
    AddTripleComponent,
    LoadingComponent,
    PrefixesComponent,
    ConfirmComponent,
    KeysPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AlertModule.forRoot(),
    CollapseModule.forRoot(),
    ModalModule.forRoot(),
    TypeaheadModule.forRoot(),
    // NouisliderModule,
    AppRoutingModule,
    HttpModule,
    DropdownModule,
    BootstrapModalModule
  ],
  providers: [GetProjectsService, QueryService, GetPatternsService, GraphService, ServerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
