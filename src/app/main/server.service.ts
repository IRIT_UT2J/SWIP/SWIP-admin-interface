import { Injectable, } from '@angular/core';

@Injectable() 
export class ServerService {
	// Use this variable to switch between local and swip server
	private url : string = 'https://swip.murloc.fr/SWIP';  //'http://localhost:8080/SWIP';  //'https://swip.murloc.fr/SWIP'; // 'http://localhost:8080/SWIP';  // 'http://localhost:8080/SWIP'; // 'http://swip.univ-tlse2.fr:8080/SWIP';
	constructor() { }

	getServerUrl() : string {
		return this.url;
	}
}