import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';
import { Observable }        from 'rxjs/Observable';
import { Subject }           from 'rxjs/Subject';

// Observable class extensions
import 'rxjs/add/observable/of';

// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

import { GetProjectsService } from '../projects/get-project.service';
import { Project } from '../projects/project';

@Component({
  selector: 'project-search',
  templateUrl: './project-search.component.html',
  styleUrls: [ './project-search.component.css' ],
  providers: [GetProjectsService]
})

export class ProjectSearchComponent implements OnInit {

  private projects: Observable<Project[]>;
  private searchTerms = new Subject<string>();
  private error: Boolean = false;

  constructor(
    private getProjectsService: GetProjectsService,
    private router: Router) {}

  /** Pushes a search term into the observable stream.
  * @param {string} term - the term that we want to match with a project name
  */
  search(term: string): void {
    this.searchTerms.next(term);
  }

  ngOnInit(): void {
    this.projects = this.searchTerms
      .debounceTime(300)        // wait 300ms after each keystroke before considering the term
      .distinctUntilChanged()   // ignore if next search term is same as previous
      .switchMap(term => term   // switch to new observable each time the term changes
        // return the http search observable
        ? this.getProjectsService.search(term)
        // or the observable of empty projects if there was no search term
        : Observable.of<Project[]>([]))
      .catch(error => {
        console.log(error);
        return Observable.of<Project[]>([]);
      });
  }
  
  /** Redirects to the specified project's page.
  * @param {Project} project - the project to redirect to
  */
  gotoDetail(project: Project): void {
    let link = ['/project', project.projectName];
    this.router.navigate(link);
  }

  /** Gets a project by its name and redirects to its page
  * @param {string} name - the name of the project to redirect to
  */
  getProject(name: string): void {
    this.getProjectsService.getProject(name)
      .subscribe(project => this.gotoDetail(project),
         error => this.error = true
      );
  }

}
