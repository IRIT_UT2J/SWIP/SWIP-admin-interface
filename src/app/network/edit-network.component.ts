import { Component, OnInit, Input } from '@angular/core';
import { GraphService } from '../network/create-graph.service';
import { GetPatternsService } from '../patterns/get-patterns.service';


@Component({
	selector: 'edit-network',
	templateUrl: './edit-network.component.html',
	styleUrls: ['./edit-network.component.css']
})

export class EditNetworkComponent {
	@Input() projectName: string;
  private triplets: boolean = true; // for testing 
	private showPopUp: boolean = false;
	private operation: string = 'node';
	private data: any;
	private new_id: string = '';
	private new_label: string = '';
	private callback: any;

	constructor(private patternsService: GetPatternsService,
              private graph: GraphService) { }

	setGraphOptions() {
      let options = {
        nodes: {
            color: '#b3cccc',
        },
        edges: {
          arrows:'to'
        },
       manipulation: {
          addNode: (data, callback) => {
            this.new_label = '';
            this.new_id = '';
            this.operation = "Add Node";
            this.data = data;
            this.callback = callback;
            this.showPopUp = true;
          },
          editNode: (data, callback) => {
            console.log("edit data&callback: ", data, callback)
            this.operation = "Edit Node";
            this.data = data;
            this.callback = callback;
            this.showPopUp = true;
          },
          addEdge: (data, callback) => {
            if (data.from == data.to) {
              var r = confirm("Do you want to connect the node to itself?");
              if (r == true)
                callback(data);
            }
            else {
              data.label = "edge label";
              callback(data);
              this.updateStorage();
            }
          },
          deleteNode: (data, callback) => {
          	callback(data);
          	this.updateStorage();
          },
          deleteEdge: (data, callback) => {
          	callback(data);
          	this.updateStorage();
          }
        }
      };
      return options;
  }

  clearPopUp() {
      this.showPopUp = false;
  }

  cancelEdit() {
      this.clearPopUp();
      this.callback(null);
  }

  saveData(new_id, new_label) {
    if (new_label != '' && new_id != '') {
      this.data.id = new_id;
      this.data.label = new_label;
      this.clearPopUp();
      if (this.operation == "Add Node" && !this.graph.getNodes().get(this.data.id)) {
          this.callback(this.data);
          this.updateStorage();
      } else if (this.operation == "Edit Node") {
      	  this.callback(this.data);
          this.updateStorage();
      } else {
          console.log("node already exists")
      }
      
    }
  }

  saveDataTriplets(id_subject, label_subject, id_object, label_object, label_predicate) {
    console.log("save triplets !")
      this.graph.addNode({id: id_subject, label: label_subject, color: '#b3cccc', font: {color:'#000', face:'arial'}});
      this.graph.addNode({id: id_object, label: label_object, color: '#b3cccc', font: {color:'#000', face:'arial'}});
      this.graph.addEdge({from: id_subject, to: id_object, arrows:'to', label: label_predicate, font: {align: 'bottom'}});

      this.patternsService.getPattern(this.projectName, 'team')
        .subscribe(pattern => {
          console.log("pattern in save data: ", pattern);
          for (let sp of pattern.subpatterns) {
            if (sp.id == 'team') {
              // let num = sp.subpatterns.length;
              // sp.subpatterns[num] = sp.subpatterns[0];
              pattern.name = 'new_name';
              console.log("new pattern : ", pattern);
              this.updatePatternStorage(pattern);
              break;
            }
          }
          this.clearPopUp();
          this.updateStorage();
        });
  }

  updatePatternStorage(pattern): void {
    let patterns = [];
    patterns.push(pattern);
    console.log("patterns in update: ", patterns);
    let str_data = JSON.stringify(patterns); // works cause theres only one pattern
    this.patternsService.setPatterns(this.projectName, pattern);
    // window.sessionStorage.setItem(this.projectName + '_patterns', str_data);
  }

  updateStorage() {
  	let str_data = JSON.stringify(this.graph.getNetworkData());
  	// window.sessionStorage.setItem(this.projectName + '_generalGraph', str_data);
  };

	ngOnInit(): void { 

	}
}