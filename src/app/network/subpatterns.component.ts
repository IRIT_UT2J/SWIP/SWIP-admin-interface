import { Component, OnInit, Input, Host } from '@angular/core';
import { Pattern } from '../patterns/pattern';
import { Subpattern } from '../patterns/subpattern';
import { PatternsComponent } from '../patterns/patterns.component';


@Component({
	selector: 'subpatterns',
	templateUrl: './subpatterns.component.html',
	styleUrls: ['./subpatterns.component.css']
})

export class SubpatternsComponent {
	@Input() pattern: Pattern;
	@Input() subpatterns: Subpattern[];

	constructor(private parent: PatternsComponent) { }

	addTripleToSp(sp : Subpattern) : void {
		console.log("i'm gonna add a triple to ", sp);
	}

	ngOnInit(): void { }
}