import { Component, OnInit, Input, Host } from '@angular/core';
import { Router } from '@angular/router';
import { Pattern } from '../patterns/pattern';
import { Subpattern } from '../patterns/subpattern';
import { PatternsComponent } from '../patterns/patterns.component';
import { GraphService } from './create-graph.service';

@Component({
	selector: 'sp-hierarchy',
	templateUrl: './sp-hierarchy.component.html',
	styleUrls: ['./sp-hierarchy.component.css']
})

export class SpHierarchyComponent implements OnInit{
	@Input() patterns: Pattern[];
	@Input() current_pattern: Pattern;

	constructor(private router: Router,
				private graph: GraphService,
				private parent: PatternsComponent) { }

	mouseOver(pattern : Pattern) : void {
		if (!this.current_pattern)
			this.graph.zoomOnPattern(pattern);
	}

	mouseLeave(pattern : Pattern) : void {
		if (!this.current_pattern)
			this.graph.resetNodesColor();
	}

	ngOnInit(): void {
	}
}