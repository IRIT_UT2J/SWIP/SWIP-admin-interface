import { Injectable } from '@angular/core';
import { Pattern } from '../patterns/pattern';
import { Subpattern } from '../patterns/subpattern';
import { QueryService } from "../queries/query.service";
import { Network, DataSet, Node, Edge, IdType } from 'vis';
import { Observable }     from 'rxjs/Observable';

import 'rxjs/add/observable/forkJoin';

@Injectable() 
export class GraphService {
	private nodes: DataSet;  // nodes of the current network
  private edges: DataSet;  // edges of the current network
  private distantKB: boolean; // check if Knowledge Base is distant or not
  private is_empty: boolean;
  private projectName: string; // projectName used in services
  private current_prefixes: string[] = []; // prefixes of current project
  private mapEdgesToTriples = {};
  private pattern_colors: string[] = ['#ff9999', '#ffcc99', '#ffff99', '#ccff99', '#99ff99',
                                      '#99ffff', '#99ccff', '#cc99ff', '#9999ff', '#ff9999'];
                                      // colors associated to patterns
	constructor(private queryService: QueryService) { }

  /**
  * Returns nodes and edges of a graph which includes all the patterns.
  * @param {Pattern[]} patterns - all patterns available from the current project
  * @param {string} project - current project name
  * @param {boolean} distant - specifies if the KB is distant or not
  */
	getGeneralGraphData(patterns : Pattern[], project: string, distant: boolean): any {
        // init variables
        this.nodes = new DataSet([]);
        this.edges = new DataSet([]);
        this.distantKB = distant;
        this.projectName = project;
        this.is_empty = true;

        let cpt_color: number = 0;
        patterns.forEach(pattern => {
          pattern.color = this.pattern_colors[cpt_color]; // associate one color per pattern
    	    this.addSubpatterns(pattern, pattern.color); // go through its subpatterns to construct the network
          cpt_color +=1;
        });

        return this.getNetworkData();
	    
	}

  /**
  * Returns nodes and edges of a graph which includes only one pattern
  * @param {Pattern} pattern - pattern selected by the user
  * @param {string} project - current project name
  * @param {boolean} distant - specifies if the KB is distant or not
  */
	getGraphData(pattern : Pattern, project: string, distant: boolean): any {
      this.nodes = new DataSet([]);
      this.edges = new DataSet([]);
      this.distantKB = distant;
      this.projectName = project;
	    this.addSubpatterns(pattern, pattern.color);
	    return this.getNetworkData();
	    
	}

  /** 
  * Goes through parent subpatterns and check for triples
  * @param {any} parent - can be a pattern or a subpattern, but not a triple
  * @param {string} color_pattern - the color of the pattern to which the subpatterns belong
  */
  addSubpatterns(parent : any, color_pattern : string) {
    parent.subpatterns.forEach( subpattern => { 
        if (!subpattern.subpatterns) // if the subpattern is a triple
            this.addLastSubpattern(subpattern, color_pattern);            
        else
            this.addSubpatterns(subpattern, color_pattern); // browse its subpatterns
    });
  }

  /** 
  * Gets the prefixes or the labels of the triple. These prefixes / labels will be shown in the graph.
  * @param {any} subsub - the subpattern to which the triple belongs
  * @param {string} color_pattern - the color of the pattern to which the subpatterns belong
  */
  addLastSubpattern(subsub, color_pattern) {
    if (this.distantKB) { // if the KB is distant, we can get the prefixes of subsub's URIs
        Observable.forkJoin(this.queryService.getUriPrefix(subsub['e1'].uri),
                            this.queryService.getUriPrefix(subsub['e2'].uri),
                            this.queryService.getUriPrefix(subsub['e3'].uri))
                  .subscribe(data => this.getLastNodesAndEdges(data, subsub, true, color_pattern));
    } else {
        Observable.forkJoin(this.queryService.getUriLabel(this.projectName, subsub['e1'].uri),
                            this.queryService.getUriLabel(this.projectName, subsub['e2'].uri),
                            this.queryService.getUriLabel(this.projectName, subsub['e3'].uri))
                  .subscribe(data => this.getLastNodesAndEdges(data, subsub, false, color_pattern));
    }
    // We need to wait for all three WS (called in getUriPrefix or getUriLabel) to be accomplished ; 
    // Observable.forkJoin calls getLastNodesAndEdges only when all the prefixes / labels are found.
  }

  /** 
  * Adds a triple to the graph : makes nodes out of subject and object, and edge out of predicate.
  * @param {any} data - an array containing the labels / prefixes of the triple. data[0] is the prefix /
  *                     label(s) of the subject, data[1] of the predicate, data[2] of the object.
  * @param {any} subsub - the subpattern to which the triple belongs
  * @param {boolean} distant - specifies if the KB is distant or not
  * @param {string} color_pattern - the color of the pattern to which the subpatterns belong
  */
  getLastNodesAndEdges(data, subsub, distant: boolean, color_pattern): void {
      if (this.is_empty)
        this.is_empty = false;

      if (!distant) { // the triple has labels
        for (let uri in data) {
          if (data[uri] instanceof Object) // data[uri] is an array containing labels
            data[uri] = data[uri][0].label.value; // we take the first label (which will be used in the graph)
        } //else, data[uri] is a fragment URI
      }

      //data is now an array of strings
      data.forEach(uri => {
        if (uri.split(':')) { // i.e. the uri has a prefix
          let prefix = uri.split(':')[0];
          if (!this.current_prefixes.includes(prefix))
            this.current_prefixes.push(prefix); // we add that prefix to the dict of prefixes if it's not already there
          }
      });

      [data[0], data[2]].forEach(elem => { // we make nodes out of the subject and the object
        if (!this.nodes.get(elem)) { // if the node doesn't already exist
            this.nodes.add({id: elem, label: elem, color: color_pattern, node_color: color_pattern, font: {color:'#000', face:'arial'}});
            if (!distant)
                this.nodes.get(elem).labels = (elem == data[0] ) ? data[0] : data[2];
        }
      });

    // if the subject / object is qualifying, we change the shape of the node
    if (subsub.e1.qualifying)
      this.nodes.update({id: data[0], label: data[0], shape: 'box'});
    if (subsub.e3.qualifying)
      this.nodes.update({id: data[2], label: data[2], shape: 'box'});

    // We add a name to each element of the triple ; this name will be used to find and edit nodes.
    // We can't use the element's ID, because two elements belonging to different subpatterns can have the same ID.
    subsub['e1'].name = data[0];
    subsub['e2'].name = data[1];
    subsub['e3'].name = data[2];

    // now that we have two nodes, we can connect them using the triple's predicate as an edge
    let edge = {id: data[1] + '_from_' + data[0] + '_to_' + data[2], from: data[0], to: data[2], arrows:'to', label: data[1], length: 100, font: {align: 'bottom'}, color: 'lightgrey'};
    this.edges.add(edge);
    this.mapEdgesToTriples[edge.id] = subsub;
  }

  /** 
  * Changes the color of all nodes contained in the specified subpattern.
  * @param {any} sp - the pattern or the subpattern which is going to be highlighted
  */
  changeNodeColor(sp : any): void {
    sp.subpatterns.forEach(subpattern => {
      if (subpattern.subpatterns) {
        this.changeNodeColor(subpattern);
      } else {
        this.nodes.update({id: subpattern.e1.name, label: subpattern.e1.name, color: '#ff3232'}); // the node is now red
        this.nodes.update({id: subpattern.e3.name, label: subpattern.e3.name, color: '#ff3232'});
      }
    });
  }

  /** 
  * Changes every graph's node color to the color of its pattern ; nothing is highlighted anymore
  */
  resetNodesColor(): void {
    this.nodes.forEach( node => {
      if (node.name) // every node is supposed to have a name, but well, shit happens
        this.nodes.update({id: node.name, label: node.name, color: node.node_color});
      else
        this.nodes.update({id: node.id, label: node.id, color: node.node_color});
    });
  }

  /** 
  * Turns all nodes into grey ; is used when a (sub)pattern is gonna be highlighted
  */
  greyNodes(): void {
    this.nodes.forEach( node => {
      if (node.name)
        this.nodes.update({id: node.name, label: node.name, color: 'lightgrey'});
      else
        this.nodes.update({id: node.id, label: node.id, color: 'lightgrey'});
    });
  }

  /** 
  * Zooms on the specified pattern. The zoom includes hightlighting the specified pattern and 
  * turning other nodes into grey
  * @param {Pattern} pattern - the pattern to zoom on
  */
  zoomOnPattern(pattern: Pattern): any {
    this.greyNodes(); 
    this.changeNodeColor(pattern);
  }

  /** 
  * Zooms on the specified subpattern. The zoom includes hightlighting the specified pattern, while 
  * the other nodes are set to their initial color
  * @param {Subpattern} subpattern - the subpattern to zoom on
  */
  zoomOnTriplets(subpattern : Subpattern): void {
    this.resetNodesColor(); 
    subpattern.subpatterns.forEach(elem => {
        if (elem.e1) { // if it's a triple
            this.nodes.update({id: elem.e1.name, label:elem.e1.name, color: '#ff3232'});  // the node is now red
            this.nodes.update({id: elem.e3.name, label: elem.e3.name, color: '#ff3232'});
        } else {
            console.log("nothing to show for now !")
        }
    });
  }

  /** 
  * Returns all the prefixes used in this project as a dict prefix / uri
  */
  getPrefixes() {
    let observableBatch = [];
    let prefixes_map = {};
    this.current_prefixes.forEach(prefix => {
      observableBatch.push(this.queryService.mapPrefixToUri(prefixes_map, prefix));
    });

    return Observable.forkJoin(observableBatch);
  }

  getTripleFromEdge(edge : Edge) {
    return this.mapEdgesToTriples[edge];
  }

  /** 
  * Returns the nodes and edges contained in the graph
  */
  getNetworkData() {
    var data = {
        nodes: this.nodes,
        edges: this.edges    
    };
    return data;
  }

  /** 
  * Returns the nodes contained in the graph
  */
  getNodes() {
  	return this.nodes;
  }

  isEmpty() : boolean {
    return this.is_empty;
  }


  /** 
  * Changes the nodes contained in the graph
  */
  setNodes(nodes: DataSet) {
    this.nodes = nodes;
  }

  /** 
  * Returns the edges contained in the graph
  */
  getEdges() {
    return this.edges;
  }

  /** 
  * Changes the edges contained in the graph
  */
  setEdges(edges: DataSet) {
    this.edges = edges;
  }

  /** 
  * Adds a new node to the graph
  */
  addNode(node: Node): void {
    this.nodes.add(node);
  }

  /** 
  * Adds a new edge to the graph
  */
  addEdge(edge: Edge): void {
    this.edges.add(edge);
  }

}