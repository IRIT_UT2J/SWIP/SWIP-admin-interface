import { Injectable, } from '@angular/core';
import { Headers, Http, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import { ServerService } from '../main/server.service';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { Pattern } from './pattern';
import { Subpattern } from './subpattern';

@Injectable() 
export class GetPatternsService {

	  private url : string = this.serverService.getServerUrl();
	  private headers = new Headers({'Content-Type': 'application/json'});

	  constructor(private http: Http,
	  			  private serverService: ServerService) { }

	  /** 
	  * Returns the patterns of the given project.
	  * @param {string} project - the name of the project to get the patterns from
	  */ 
	  getPatterns(project: string) : Observable<Pattern[]> {
	    return this.http.get(`${this.url}/${project}/getPatterns`)
	        .map((res:Response) => res.json().patterns as Pattern[])
	        .catch((err:any) => this.handleError());
     }

     /** 
	 * Returns the pattern of the given project which has the specified name.
	 * @param {string} project - the name of the project to get the pattern from
	 * @param {string} patternName - the name of the pattern we want to get
	 */ 
     getPattern(project: string, patternName: string): Observable<Pattern> {
     	return this.getPatterns(project)
    	.mergeMap(processArray => {
	      let array = processArray.filter(pat => pat.name === patternName);
	      if (array.length === 0) 
	      	return Observable.throw('pattern not found');
	      console.log(" get pattern : ", array);
	      return array;
	    })
	    .catch((err:any) => this.handleError());
     }

     /** 
	 * Changes all the patterns of the given project.
	 * @param {string} project - the name of the project
	 * @param {any} patterns - the patterns that are going to be set for the given project
	 */ 
     setPatterns(project: string, patterns): any {
          console.log("S E T T I N G PATTERNS ", patterns);
     	let str =  'patterns=' + JSON.stringify(patterns);
     	var headers = new Headers();
      	headers.append('Content-Type', 'application/x-www-form-urlencoded');
     	return this.http.post(`${this.url}/${project}/setPatterns`, str, {headers: headers})
     	.map(res => console.log("ws : ", res))
     	.catch((err:any) => this.handleError());
     	// .subscribe();
     }

     getSubpatternsFromPattern(pattern : Pattern) : any {
     	let array_subpatterns = [];
     	pattern.subpatterns.forEach(sp => {
     		array_subpatterns.push(sp.id);
     		sp.subpatterns.forEach(ssp => array_subpatterns = this.getSubpatternsFromSubpattern(ssp, array_subpatterns))
     	});
     	return array_subpatterns;
     }

     getSubpatternsFromSubpattern(subpattern, array_subpatterns) : any {
     	if (subpattern.id) {
     		array_subpatterns.push(subpattern.id);
     		return this.getSubpatternsFromSubpattern(subpattern.subpatterns, array_subpatterns);
     	} else {
     		return array_subpatterns;
     	}
     }

     getSubpatternById(element: any, subpattern : string) : any { // element = pattern at first
     	for (let sp in element.subpatterns) {
     		if (element.subpatterns[sp].id == subpattern)
     			return element.subpatterns[sp];
     		else {
     			if (element.subpatterns[sp].subpatterns)
     				return this.getSubpatternById(element.subpatterns[sp], subpattern)
     		}
     	}
     }

     /** 
	 * Prints an error log and returns an undefined value.
	 */
     handleError() : Observable<any> {
		console.log("An error occurred..."); // needs to be a slight bit more explicit
		return Observable.of(undefined);
	}
}