// with subsubsub pattern :

this.patterns = {
            "patterns": [{
                    "sentenceTemplate": "-1- -for-team-[\" is part of team \"-3-\", \"]",
                    "subpatterns": [{  // subpatterns 1.0
                        "pivotElement": "[KbPatternElement]http://swip.univ-tlse2.fr/TEST#Person - id=1 - qualifying=true",
                        "maxOccurences": 2,
                        "minOccurences": 0,
                        "id": "sp 1",
                        "subpatterns": [{  // subpatterns 2
                            "pivotElement": "[KbPatternElement]http://swip.univ-tlse2.fr/TEST#Person - id=1 - qualifying=true",
                            "maxOccurences": 2,
                            "minOccurences": 0,
                            "id": "team",
                            "subpatterns": [{  // subpatterns 3
                                "e1": {
                                    "qualifying": true,
                                    "mappingCompulsory": false,
                                    "maxOccurences": 1,
                                    "id": 1,
                                    "uri": "http://swip.univ-tlse2.fr/TEST#Person"
                                },
                                "e2": {
                                    "qualifying": true,
                                    "mappingCompulsory": false,
                                    "maxOccurences": 1,
                                    "id": 2,
                                    "uri": "http://swip.univ-tlse2.fr/TEST#isPartOf"
                                },
                                "e3": {
                                    "qualifying": true,
                                    "mappingCompulsory": false,
                                    "maxOccurences": 1,
                                    "id": 3,
                                    "uri": "http://swip.univ-tlse2.fr/TEST#Team"
                                }
                            }] // end of subpatterns 3
                        }]  // end of subpatterns 2
                    },
                    {  // subpatterns 1.1
                        "pivotElement": "[KbPatternElement]http://swip.univ-tlse2.fr/TEST#Person - id=1 - qualifying=true",
                        "maxOccurences": 2,
                        "minOccurences": 0,
                        "id": "sp 2",
                        "subpatterns": [{  // subpatterns 3
                                "e1": {
                                    "qualifying": true,
                                    "mappingCompulsory": false,
                                    "maxOccurences": 1,
                                    "id": 1,
                                    "uri": "http://swip.univ-tlse2.fr/TEST#Person"
                                },
                                "e2": {
                                    "qualifying": true,
                                    "mappingCompulsory": false,
                                    "maxOccurences": 1,
                                    "id": 2,
                                    "uri": "http://swip.univ-tlse2.fr/TEST#isPartOf"
                                },
                                "e3": {
                                    "qualifying": true,
                                    "mappingCompulsory": false,
                                    "maxOccurences": 1,
                                    "id": 3,
                                    "uri": "http://swip.univ-tlse2.fr/TEST#Team"
                                }
                            }] // end of subpatterns 3
                    }], // end of subpatterns 1
                    "name": "team"
                },
                {
                    "sentenceTemplate": "SENTENCE TEMPLATE TEST",
                    "subpatterns": [{
                        "pivotElement": "[KbPatternElement]http://swip.univ-tlse2.fr/TEST#Person - id=1 - qualifying=true",
                        "maxOccurences": 2,
                        "subpatterns": [{
                                "e1": {
                                    "qualifying": true,
                                    "mappingCompulsory": false,
                                    "maxOccurences": 1,
                                    "id": 1,
                                    "uri": "http://swip.univ-tlse2.fr/TEST#Person"
                                },
                                "e2": {
                                    "qualifying": true,
                                    "mappingCompulsory": false,
                                    "maxOccurences": 1,
                                    "id": 2,
                                    "uri": "http://swip.univ-tlse2.fr/TEST#isPartOf"
                                },
                                "e3": {
                                    "qualifying": true,
                                    "mappingCompulsory": false,
                                    "maxOccurences": 1,
                                    "id": 3,
                                    "uri": "http://swip.univ-tlse2.fr/TEST#Team"
                                }
                            },
                            {
                                "e1": {
                                    "qualifying": true,
                                    "mappingCompulsory": false,
                                    "maxOccurences": 1,
                                    "id": 1,
                                    "uri": "http://swip.univ-tlse2.fr/TEST#Person"
                                },
                                "e2": {
                                    "qualifying": true,
                                    "mappingCompulsory": false,
                                    "maxOccurences": 1,
                                    "id": 2,
                                    "uri": "http://swip.univ-tlse2.fr/TEST#isPartOf"
                                },
                                "e3": {
                                    "qualifying": true,
                                    "mappingCompulsory": false,
                                    "maxOccurences": 1,
                                    "id": 3,
                                    "uri": "http://swip.univ-tlse2.fr/TEST#Team"
                                }
                            }
                        ],
                        "minOccurences": 0,
                        "id": "team"
                    }],
                    "name": "TEST"
                }
            ]
        }.patterns;



// with two patterns :

        this.patterns = {
            "patterns": [{
                "sentenceTemplate": "-1- -for-team-[\" is part of team \"-3-\", \"]",
                "subpatterns": [{
                    "pivotElement": "[KbPatternElement]http://swip.univ-tlse2.fr/TEST#Person - id=1 - qualifying=true",
                    "maxOccurences": 2,
                    "subpatterns": [{
                        "e1": {
                            "qualifying": true,
                            "mappingCompulsory": false,
                            "maxOccurences": 1,
                            "id": 1,
                            "uri": "http://swip.univ-tlse2.fr/TEST#Person"
                        },
                        "e2": {
                            "qualifying": true,
                            "mappingCompulsory": false,
                            "maxOccurences": 1,
                            "id": 2,
                            "uri": "http://swip.univ-tlse2.fr/TEST#isPartOf"
                        },
                        "e3": {
                            "qualifying": true,
                            "mappingCompulsory": false,
                            "maxOccurences": 1,
                            "id": 3,
                            "uri": "http://swip.univ-tlse2.fr/TEST#Team"
                        }
                    }],
                    "minOccurences": 0,
                    "id": "team"
                }],
                "name": "team"
            },
            {
                "sentenceTemplate": "SENTENCE TEMPLATE TEST",
                "subpatterns": [{
                    "pivotElement": "[KbPatternElement]http://swip.univ-tlse2.fr/TEST#Person - id=1 - qualifying=true",
                    "maxOccurences": 2,
                    "subpatterns": [{
                        "e1": {
                            "qualifying": true,
                            "mappingCompulsory": false,
                            "maxOccurences": 1,
                            "id": 1,
                            "uri": "http://swip.univ-tlse2.fr/TEST#Person"
                        },
                        "e2": {
                            "qualifying": true,
                            "mappingCompulsory": false,
                            "maxOccurences": 1,
                            "id": 2,
                            "uri": "http://swip.univ-tlse2.fr/TEST#isPartOf"
                        },
                        "e3": {
                            "qualifying": true,
                            "mappingCompulsory": false,
                            "maxOccurences": 1,
                            "id": 3,
                            "uri": "http://swip.univ-tlse2.fr/TEST#Team"
                        }
                    },
                    {
                        "e1": {
                            "qualifying": true,
                            "mappingCompulsory": false,
                            "maxOccurences": 1,
                            "id": 1,
                            "uri": "http://swip.univ-tlse2.fr/TEST#Person"
                        },
                        "e2": {
                            "qualifying": true,
                            "mappingCompulsory": false,
                            "maxOccurences": 1,
                            "id": 2,
                            "uri": "http://swip.univ-tlse2.fr/TEST#isPartOf"
                        },
                        "e3": {
                            "qualifying": true,
                            "mappingCompulsory": false,
                            "maxOccurences": 1,
                            "id": 3,
                            "uri": "http://swip.univ-tlse2.fr/TEST#Team"
                        }
                    }],
                    "minOccurences": 0,
                    "id": "team"
                }],
                "name": "TEST"
            }]
        }.patterns;