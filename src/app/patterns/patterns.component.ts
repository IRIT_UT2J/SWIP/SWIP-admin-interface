import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Router } from '@angular/router';
import { GetProjectsService } from '../projects/get-project.service';
import { Project } from '../projects/project';
import { Pattern } from './pattern';
import { Subpattern } from './subpattern';
import { GetPatternsService } from './get-patterns.service';
import { GraphService } from '../network/create-graph.service';
import { SpHierarchyComponent } from '../network/sp-hierarchy.component';
import { EditNetworkComponent } from '../network/edit-network.component'; 
import { AddPatternComponent } from '../modals/add-pattern.component'; 
import { AddSubpatternComponent } from '../modals/add-subpattern.component'; 
import { AddTripleComponent } from '../modals/add-triple.component'; 
import { LoadingComponent } from '../modals/loading.component'; 
import { PrefixesComponent } from '../modals/prefixes.component'; 
import { Network, DataSet, Node, Edge, IdType } from 'vis';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'patterns',
  templateUrl: './patterns.component.html',
  styleUrls: ['./patterns.component.css']
})

export class PatternsComponent implements OnInit {
  @ViewChild(EditNetworkComponent) edit_network: EditNetworkComponent;
  @ViewChild('loading') public loading: ModalDirective;  // rename as loadingModal
  @ViewChild('prefixes') public prefixes: ModalDirective;
  @ViewChild('confirm') public confirm: ModalDirective;

  patterns: Pattern[];
  @Input() project: Project;
  private current_pattern: Pattern;
  private clicked_node: any;
  private current_network: any;
  private current_prefixes: any;
  private show_only_pattern: boolean = false;
  private show_prefixes: boolean = false;
  private loaded: boolean = false;
  private selected_triple: any;
  private host_pattern; // or subpattern
  private host_triple;
  private new_pattern;

  private show_confirm: boolean = false;
  private confirm_message: string = '';
  private confirm_title: string = '';
  private confirm_callback: any;
  private confirm_param: any;

  constructor(private getPatternsService: GetPatternsService,
              private getProjectsService: GetProjectsService,
              private graph: GraphService,
              private route: ActivatedRoute) { }


  /* --- GET GRAPHS --- */ 


  /** 
  * Gets the graph of triples from a given pattern.
  * @param {Pattern} pattern - the pattern from which we want a graph
  */
  getGraph(pattern: Pattern) {
    let container_triplets = document.getElementById('graph'); // div containing the graph
    let data = this.graph.getGraphData(pattern, this.project.projectName, this.project.distant); // nodes and edges of the graph
    this.current_network = new Network(container_triplets, data, {interaction: {
        navigationButtons: true,
        keyboard: true
    }}); // build a network from the data and enable zoom in & out buttons
    console.log("new network : ", this.current_network);
    this.current_pattern = pattern; // focus on the given pattern
    this.listenToEvents(false);
  }

  /** 
  * Gets the graph of triples from every pattern in the project.
  */
  getGeneralGraph() {
    console.log("i'm getting the general graph !")
    console.log("before patterns : ", this.patterns);
    this.loading.show();
    this.current_pattern = null; // no patterns are focused on anymore
    this.getPatternsService.getPatterns(this.project.projectName).subscribe(patterns => {
      console.log("after patterns : ", patterns);
      if (!patterns) {
        this.loading.hide();
        console.log("no patterns found !");
        return;
      }
      this.patterns = patterns;
      let data = this.graph.getGeneralGraphData(patterns, this.project.projectName, this.project.distant);
      this.current_network = new Network(document.getElementById('graph'), data, {interaction: {
          navigationButtons: true,
          keyboard: true
      }});
      this.graph.resetNodesColor(); 
      this.listenToEvents(true);
      if (this.graph.isEmpty()) {
        this.loading.hide();
        console.log("no triples yet !");
      }
      console.log("patterns : ", this.patterns);
    });
  }

  /* --- /.GET GRAPHS --- */ 


  /** 
  * Listens to events on the current network : a node clicked, or the stabilization of the network
  * @param {Pattern} pattern - the pattern from which we want a graph
  */
  listenToEvents(isGeneralGraph: boolean): void {
    this.current_network.on('click', properties => { // whenever a node is clicked
        if (properties.edges.length > 0) {
          let triple = this.graph.getTripleFromEdge(properties.edges[0]);
          this.selected_triple = triple;
          console.log("related triple : ", triple);
        }
    });

    this.current_network.once('stabilized', iterations => {  // when the graph has finished to load
        console.log("patterns after graph : ", this.patterns);
        this.loading.hide(); // delete the "loading network" message
        // let options = this.edit_network.setGraphOptions(); // allows 'edit network' options
        // this.current_network.setOptions(options); // edit network only when it's stabilized
        if (this.project.distant) { // prints a table of prefixes and their matching URI
          this.graph.getPrefixes().subscribe(data => {
            this.current_prefixes = data[0];
          }); // change that ugly thing
        }

    });
  }

  /* --- ZOOM ON NODES --- */ 

  /** 
  * Zooms on a given pattern. If the 'show_only_pattern' option is true, the graph only shows
  * the given pattern.
  * @param {Pattern} pattern - the pattern to zoom on
  */
  focusOnPattern(pattern : Pattern) {
      if (!this.show_only_pattern) {
        this.current_pattern = pattern;
        this.zoom(pattern, true);
      }
      else 
        this.getGraph(pattern);          
  }

  /** 
  * Zooms on a given subpattern. If the graph was only showing a pattern to which the specified subpattern didn't
  * belong, the graph now shows the pattern which contains the specified pattern.
  * @param {Pattern} pattern - the pattern which contains the subpattern
  * @param {Subpattern} subpattern - the subpattern to zoom on
  */
  focusOnSp(pattern: Pattern, sp: Subpattern): void {
    if (pattern != this.current_pattern && this.show_only_pattern)
      this.getGraph(pattern);
    this.zoom(sp, false);
  }

  /** 
  * Zooms on a element : the zomm changes depending on the type of the element (pattern or not).
  * @param {any} element - the element to zoom on, which can be a pattern or a subpattern
  * @param {boolean} isPattern - specifies if the given element is a Pattern or not
  */
  zoom(element: any, isPattern: boolean): void {
    if (isPattern)
      this.graph.zoomOnPattern(element);
    else
      this.graph.zoomOnTriplets(element);

    if (element.subpatterns[0]) {  // check if it has at least one node to zoom on
      let groupNodes = this.getNodesIds(element, []);
      this.current_network.fit({
        nodes: groupNodes,
          animation: {
          duration: 300
        }
      });
    }
      
    // this.current_network.focus(this.getNodesIds(element), options); // zooms on the first triple of the element
  }

  /** 
  * Gets the ids of every node contained in the specified element.
  * @param {any} element - the element to get the nodes ids from. This can be a pattern or a subpattern.
  * @param {any} array - the array which is going to contain the nodes ids, initially empty.
  */
  getNodesIds (element: any, array) : string[] {
    element.subpatterns.forEach(sp => {
      if (sp.e1) {
        array.push(sp.e1.name, sp.e3.name);
      } else {
        if (sp.subpatterns)
          return this.getNodesIds(sp, array);
      }
    });
    return array;
  }

  /* --- /.ZOOM ON NODES --- */ 


  /** 
  * Toggles the 'show_only_pattern' value.
  * Focuses on the current pattern when 'show_only_pattern' is true ;
  * else, show the general graph.
  */
  showOnlyPattern() : void {
    this.show_only_pattern = !this.show_only_pattern;
    if (this.current_pattern)
      this.focusOnPattern(this.current_pattern);
    if (!this.show_only_pattern)
      this.getGeneralGraph();
  }

  showAddTriple(subpattern: Subpattern) : void {
    this.host_triple = subpattern;
    // this.add_triple.show();
  }

  showAddSubpattern(element: any) : void {
    this.host_pattern = element;
    console.log("host pattern : ", element);
  }

  hideModals() : void {
    this.host_pattern = undefined;
    this.host_triple = undefined;
    this.new_pattern = undefined;
    this.show_confirm = false;
  }

  confirmDelete(element : any) : void {
    if (element.name) { // it's a pattern, i'll change that later
      this.confirm_title = " delete this pattern";
      this.confirm_message = "All the subpatterns and triples contained by the pattern will also be lost.";
      this.confirm_callback = 'deletePattern';
      this.confirm_param = element;
    } else if (element.id) { // it's a subpattern
      this.confirm_title = " delete this subpattern";
      this.confirm_message = "All the subpatterns and triples contained by the subpattern will also be lost.";
      this.confirm_callback = 'deleteSubpattern';
      this.confirm_param = element;
    } else {  // it's a triple
      this.confirm_title = " delete this triple";
      this.confirm_message = "If the triple is the pivot element of a subpattern, the subpattern is also going to be deleted.";
      this.confirm_callback = 'deleteTriple';
      this.confirm_param = element;
    }
    
    this.show_confirm = true;
  }

  deletePattern(pattern : Pattern) : void {
    this.show_confirm = false;
    for (let pat of this.patterns) {
      if (pat.name == pattern.name) {
        let index = this.patterns.indexOf(pat);
        this.patterns.splice(index, 1);
        break;
      }
    };

    console.log("new patterns : ", this.patterns)
    this.getPatternsService.setPatterns(this.project.projectName, {patterns: this.patterns})
      .subscribe(stuff => {
        this.getGeneralGraph()
      });
  }

  deleteTriple(triple) : void { // WARNING  : also delete sp if triple is the last one
    this.show_confirm = false;
    for (let pattern of this.patterns) {
      if (pattern.subpatterns)
        this.searchTriple(pattern, triple);
    }
    console.log("new patterns : ", this.patterns);
    this.getPatternsService.setPatterns(this.project.projectName, {patterns: this.patterns})
    .subscribe(stuff => {
      this.selected_triple = undefined;
      this.getGeneralGraph();
    });
  }

  searchTriple(element: any, triple) {
    element.subpatterns.forEach( item => {
        if (item.subpatterns)
          this.searchTriple(item, triple); 
        else { // it's a triple
          if (item.e1.name == triple.e1.name && item.e2.name == triple.e2.name && item.e3.name == triple.e3.name) {
            console.log("in equal !")
            let index = element.subpatterns.indexOf(item);
            element.subpatterns.splice(index, 1);
            console.log("new element : ", element);
            if (element.subpatterns.length == 0)
              this.deleteSubpattern(element, false);
          }
        }
      });
  }

  deleteSubpattern(subpattern : Subpattern, saveChanges: boolean = true) : void {
    this.show_confirm = false;
    for (let pattern of this.patterns) {
      this.filterSubpatterns(pattern, subpattern);
    };

    console.log("new patterns : ", this.patterns)
    if (saveChanges) {
      this.getPatternsService.setPatterns(this.project.projectName, {patterns: this.patterns})
      .subscribe(stuff => {this.getGeneralGraph()});
    }
  }

  filterSubpatterns(element: any, subpattern : Subpattern) {
    element.subpatterns.forEach( item => {
        if (item.subpatterns)
          this.filterSubpatterns(item, subpattern);
        if (item.id == subpattern.id) {
          let index = element.subpatterns.indexOf(item);
          element.subpatterns.splice(index, 1);
        }
      });
  }

  /* --- TOGGLE TREE VIEW (SUBPATTERNS HIERARCHY) --- */ 

  /** 
  * When the page unloads, closes the treeview of patterns.
  */
  resetTreeViewOnUnload(): void {
    window.addEventListener("beforeunload", event => {
      this.resetTreeView();
    });
  }

  /** 
  * Closes the treeview of patterns.
  */
  resetTreeView(): void {
    this.patterns.forEach(pattern => {
      pattern.show_details = false;
      pattern.show_edit_options = false;
      pattern.subpatterns.forEach(sp => {
        this.resetSpTreeView(sp);
      });
    });
    // this.getPatternsService.setPatterns(this.project.projectName, this.patterns);
  }

  /** 
  * Doesn't show the details of a given subpattern.
  * @param {Subpattern} sp - the subpattern of which the details are hidden
  */
  resetSpTreeView(sp: Subpattern): void {
      sp.show_details = false;
      if (sp.subpatterns) {
        sp.subpatterns.forEach(ssp => {
          this.resetSpTreeView(ssp);
        });
      }
  }

  /* --- /.TOGGLE TREE VIEW (SUBPATTERNS HIERARCHY) --- */ 


  /* --- GET PARAMETERS ON INIT --- */ 

  ngOnInit(): void {
    this.route.params.subscribe(
      (params : Params) => {
          this.getProjectsService.getProject(params["projectName"])
            .subscribe(project => {
                this.project = project;  // get the current project
                console.log("project : ", this.project)
                this.getGeneralGraph(); // get the graph representing the triples of all the patterns
                //   this.getPatternsService.getPatterns(this.project.projectName).subscribe(patterns => {
                //     this.patterns = patterns;  // get all the project's patterns
                //     console.log("PATTERNS : ", this.patterns)
                //     // let new_pattern = {patterns: [{"sentenceTemplate":"-1- -for-team-[\" is part of team \"-3-\", \"]","subpatterns":[{"pivotElement":{"qualifying":true,"mappingCompulsory":false,"maxOccurences":1,"canonicaleType":"fr.irit.swip2.pivottomappings.model.patterns.patternElement.ClassPatternElement","id":1,"uri":"http://swip.univ-tlse2.fr/TEST#Person"},"maxOccurences":2,"canonicaleType":"fr.irit.swip2.pivottomappings.model.patterns.subpattern.SubpatternCollection","subpatterns":[{"canonicaleType":"fr.irit.swip2.pivottomappings.model.patterns.subpattern.PatternTriple","e1":{"qualifying":true,"mappingCompulsory":false,"maxOccurences":1,"canonicaleType":"fr.irit.swip2.pivottomappings.model.patterns.patternElement.ClassPatternElement","id":1,"uri":"http://swip.univ-tlse2.fr/TEST#Person"},"e2":{"qualifying":true,"mappingCompulsory":false,"maxOccurences":1,"canonicaleType":"fr.irit.swip2.pivottomappings.model.patterns.patternElement.PropertyPatternElement","id":2,"uri":"http://swip.univ-tlse2.fr/TEST#isPartOf"},"e3":{"qualifying":true,"mappingCompulsory":false,"maxOccurences":1,"canonicaleType":"fr.irit.swip2.pivottomappings.model.patterns.patternElement.ClassPatternElement","id":3,"uri":"http://swip.univ-tlse2.fr/TEST#Team"}}],"minOccurences":0,"id":"team"}],"name":"team"}]};
                //     // this.getPatternsService.setPatterns(this.project.projectName, new_pattern);
                //     // if (this.patterns)
                //     this.getGeneralGraph(); // get the graph representing the triples of all the patterns
                // });
            });
      });
    this.resetTreeViewOnUnload(); // listens for the 'beforeunload' event to close the treeview of patterns
  }

  /* --- /.GET PARAMETERS ON INIT --- */ 
}