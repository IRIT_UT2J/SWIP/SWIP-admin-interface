import { Subpattern } from './subpattern'

export class Pattern {
	sentenceTemplate: string;
	subpatterns: Subpattern[];
	name: string;
	show_details: boolean = false;
	show_edit_options: boolean = false;
	color: string;
}