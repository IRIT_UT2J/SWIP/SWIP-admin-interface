export class Subpattern {
	pivotElement: any;
	maxOccurences: number;
	subpatterns: any; // MAKE A SUBCLASS OUTTA THIS
	minOccurences: number;
	id: string;
	show_details: boolean = false;
	show_edit_options: boolean = false;
}